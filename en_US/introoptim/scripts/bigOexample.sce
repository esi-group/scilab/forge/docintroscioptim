// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

x = linspace(-0.1,0.1,1000);
y = 2*x.^2 + 5*x.^3 - 4 *x.^4;
yO=11*x.^2;
plot(x,y,"b-")
plot(x,yO,"r-")
legend(["2x^2+5x^3-4x^4" "11x^2"]);
xtitle("Big O Notation","X","");
