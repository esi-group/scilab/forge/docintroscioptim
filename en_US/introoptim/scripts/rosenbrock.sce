// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// rosenbrock --
//   Compute the Rosenbrock cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=1, returns the value of f,
//  * if ind=2, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   "An Automatic Method for Finding the Greatest or Least Value of a Function"
//   H. H. Rosenbrock, 1960, The Computer Journal
//
// Notes:
//   Starting point is x0 = [-1.2 1.0]
//   f(x0)=   24.2
//   Minimum is xopt = [1 1]
//   f(x0)=   0.
//   
function [ f , g , H ] = rosenbrock ( x )
  f = 100*(x(2)-x(1)^2)^2 + (1-x(1))^2
  g(1) = -400*(x(2)-x(1)^2)*x(1) - 2*(1-x(1))
  g(2) = 200*(x(2)-x(1)^2)
  H(1,1) = 1200 * x(1)^2 - 400 * x(2) + 2
  H(1,2) = -400 * x(1)
  H(2,1) = H(1,2)
  H(2,2) = 200
endfunction

// Checking starting point
x0 = [-1.2 1.0]'
f0 = rosenbrock ( x0 )
// Check function value at optimum
xopt = [1.0 1.0]'
fopt = rosenbrock ( xopt )

// Check derivatives at starting point
[ f , g , H ] = rosenbrock ( x0 )
[gfd , Hfd ] = derivative(rosenbrock , x0 , H_form='blockmat')
norm(g-gfd')/norm(g)
norm(H-Hfd)/norm(H)


xopt = [1.0 1.0]';
[ f , g , H ] = rosenbrock ( xopt );
mprintf ( "Computed f(x) = %f (expected = %f)\n", f, 0.0 );
mprintf ( "Computed g(x) = \n");disp(g');
mprintf ( "Computed H(x) = \n");disp(H);
[gfd , Hfd ] = derivative(rosenbrock , xopt , H_form='blockmat');
mprintf ( "Finite Differences g(x) = \n");disp(gfd);
mprintf ( "Finite Differences H(x) = \n");disp(Hfd);



// Plot the contour, but faster
function f = rosenbrockC ( x1 , x2 )
  f = rosenbrock ( [x1,x2]' )
endfunction

xdata = linspace ( -2 , 2 , 100 );
ydata = linspace ( -1 , 2 , 100 );
contour ( xdata , ydata , rosenbrockC , [2 10 100 500 1000 2000] )

// Plot the surface

function f = rosenbrockS ( x1 , x2 )
  f = rosenbrock ( [ x1 x2 ] )
endfunction

scf();
x = linspace ( -2 , 2 , 20 );
y = linspace ( -1 , 2 , 20 );
Z = (feval ( x , y , rosenbrockS ))';
surf(x,y,Z)
h = gcf();
cmap=graycolormap(10)
h.color_map = cmap;

// Check the hessian at optimum
spec(H)

// Plot the contour at optimum
x = linspace ( 0.9 , 1.1 , 100 );
y = linspace ( 0.9 , 1.1 , 100 );
contour ( x , y , rosenbrockC , [0.1 1 2 4] )


function f = rosenbrockS2 ( x1 , x2 )
  f = rosenbrock ( [ x1 x2 ] )
endfunction

scf();
x = linspace ( -2 , 2 , 20 );
y = linspace ( -1 , 2 , 20 );
Z=feval ( x , y , rosenbrockS2 );
plot3d(x,y,Z)
h = gcf();
cmap=graycolormap(10)
h.color_map = cmap;

// Compute the eigenvalues at (1,1)
x = [1 1];
[ f , g , H ] = rosenbrock ( x );
D = spec ( H )

// Compute the eigenvalues at (0,1)
x = [0 1];
[ f , g , H ] = rosenbrock ( x );
D = spec ( H )

// Find a point where the Hessian matrix is not definite positive
rand("seed",0);
ntrials = 100;
low = [-2 -1]';
upp = [2 2]';
for it = 1 : ntrials
  t = rand(2,1);
  x = (upp - low) .* t + low;
  [ f , g , H ] = rosenbrock ( x );
  D = spec ( H );
  mprintf("(%3d) x=[%+5f %+5f], D =[%+7.1f %+7.1f]\n",it,x(1),x(2),D(1),D(2))
end

x = [0 1];
[ f , g , H ] = rosenbrock ( x );
D = spec ( H )

