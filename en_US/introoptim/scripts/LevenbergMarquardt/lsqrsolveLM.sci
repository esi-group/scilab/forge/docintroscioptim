// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [xopt,fopt,gopt,ropt,status,niter,nevalf] = lsqrsolveLM(__LMcostf__,x0,maxiter,maxfevals,atolx,rtolx,atolg,verbose)
    // Minimizes the sum of squares sum(r.^2)
    //
    // Caution !!
    // This is a research project: this routine is nothing but robust!!!
    //
    // Implementation notes
    //
    // The modDecr variable contains the decrease of the model 
    // ||r(x)||^2 - ||r(x)+J*p||^2
    // This can be computed with:
    //     modDecr = fprev - sum((r + J*p).^2)
    // or with:
    //     modDecr = 2*p'*(mu*p-gopt)
    // This variable is the denominator of the gain ratio 
    // GR = (||r(x)||^2 - ||r(x+p)||^2)/(||r(x)||^2 - ||r(x)+J*p||^2)
    // Because of roundoff errors, the denominator can be negative.
    // We use the expression of ||r(x)||^2 - ||r(x)+J*p||^2 given by 
    // Moré (1977), which leads to:
    //     funDecr = fprev - fnew
    //     modDecr = sum((J*p).^2) + 2*mu*sum((D*p).^2)
    // But the denominator can overflow.
    // This is why we factor this by fprev, so that 
    // no overflow can occur, and the denominator is non-negative, 
    // regardless of roundoff errors.
    //
    // The strategy for increasing mu is taken from Madsen, Nielsen and Tingleff.
    // When we increase mu because the linear model and the function 
    // do not agree, we do 
    //     mu=mu*nu 
    //     nu=nu*2
    // instead of the straightforward 
    //     mu=mu*2
    // This allows to increase mu fast enough. 
    // When the linear model and the function agree, 
    // then we set 
    //     nu=2
    // so that the next increasing of mu starts with the nu=2 factor.
    //
    // During the resolution of the linear system, 
    // we Temporarily disable warnings with 
    //     m = warning("query")
    //     warning("off")
    //     [...]
    //     warning(m)
    // This avoids warning messages about the conditioning of the matrix:
    //     matrix is close to singular or badly scaled. rcond =    2.8009D-17
    // or the rank deficiency:
    //     Rank deficient. rank = 1
    // These two problems are expected issues, so we choose just to 
    // ignore them.
    // 
    // In order to compute the direction p, we could solve the 
    // normal equations:
    //     B = A+2*mu*D^2
    //     p = B\-g
    // where A = 2*J'*J and g=2*J'*r
    // Instead, we solve the linear least squares problem:
    //     AL = [Jopt;sqrt(mu)*D]
    //     bL = [-ropt;zeros(n,1)]
    //     p = AL\bL
    // This is because the matrix A+mu*D^2 generally has 
    // a condition number which is twice the condition number 
    // of the least squares problem.
    // For details, see Moré.
    // 
    // References
    // The Levenberg-Marquardt algorithm: Implementation and theory
    // Jorge J. Moré
    // Lecture Notes in Mathematics, 1978, Volume 630/1978, 105-116
    //
    // Methods for non-linear least squares problems
    // 2nd Edition, April 2004
    // K. Madsen, H.B. Nielsen, O. Tingleff
    // Informatics and Mathematical Modelling
    // Technical University of Denmark

    //
    // Initialize
    x0 = x0(:)
    n = size(x0,"*")
    xopt = x0
    status = "continue"
    niter = 0
    nu = 2
    p = []
    //
    // Compute function value
    nevalf = 1

    [ropt,Jopt] = lsqrsolveLM_evalF (__LMcostf__,xopt)
    D = lsqrsolveLM_scaleMatrix(Jopt)
    m = size(ropt,"*")
    if ( size(ropt,"r")<>1 & size(ropt,"c")<>1 ) then
        lclmsg = "%s: Wrong size for difference vector r: %d-by-%d or "+...
            "%d-by-%d matrix expected.\n"
        error(msprintf(gettext(lclmsg),"lsqrsolveLM",m,1,1,m))
    end
    if ( size(Jopt)<>[m n] ) then
        lclmsg = "%s: Wrong size for Jacobian: %d-by-%d matrix expected.\n"
        error(msprintf(gettext(lclmsg),"lsqrsolveLM",m,n))
    end
    [fopt,gopt,A] = lsqrsolveLM_computefgA(ropt,Jopt)
    mu = max(diag(A))
    fprev = fopt
    //
    // Compute function and model decrease
    // Compute the gain ratio GR
    GR = %inf
    //
    // Nogain : number of consecutive iterations without 
    // any improvement in the function value.
    nogainMax = 10
    nogain = 0
    while (%t)
        niter = niter + 1
        if ( verbose ) then
            lsqrsolveLM_outputFun(niter,nevalf,fopt,gopt,Jopt,p,mu)
        end
        [stop,status] = lsqrsolveLM_terminate(niter,maxiter,nevalf,maxfevals,..
            xopt,p,fopt,atolx,rtolx,atolg)
        if ( stop ) then
            break
        end
        //
        p = lsqrsolveLM_computedir3(Jopt,mu,D,ropt,gopt)
        xnew = xopt + p
        //
        nevalf = nevalf + 1
        [rnew,Jnew] = lsqrsolveLM_evalF (__LMcostf__,xnew)
        fnew = sum(rnew.^2)
        //
        GR = lsqrsolveLM_gainratio3(fopt,fnew,ropt,Jopt,D,p)
        //
        // Update x, SS, r and g
        if ( GR > 0 ) then
            xprev = xopt
            fprev = fopt
            //
            xopt = xnew
            ropt = rnew
            Jopt = Jnew
            [fopt,gopt,A] = lsqrsolveLM_computefgA(ropt,Jopt)
            nogain = 0
            D = lsqrsolveLM_scaleMatrix(Jopt)
        else
            nogain = nogain + 1
            if ( nogain > nogainMax ) then
                status = "nofunimprove"
                lclmsg = "%s: Unable to improve function value.\n"
                warning(msprintf(gettext(lclmsg),"lsqrsolveLM"))
                break
            end
        end
        if ( GR < 0.25 ) then
            mu = mu * nu
            nu = 2 * nu
        elseif ( GR > 0.75 ) then
            // We could set mu = mu/3
            // but we follow Madsen, Nielsen, Tingleff
            mu = mu * max(1/ 3,1-(2*GR-1)^3)
            nu = 2
        end
    end
    // Check the rank
    rk=rank(Jopt,sqrt(%eps))
    if ( rk <> n ) then
        lclmsg = "%s: Jacobian is rank deficient.\n"
        warning(msprintf(gettext(lclmsg),"lsqrsolveLM"))
    end

endfunction
function [f,g,A] = lsqrsolveLM_computefgA(r,J)
    // Given the residual vector r and the Jacobian matrix J,
    // compute the function value, the gradient and the 
    // first part of the Hessian.
    A = 2*J'*J
    g = 2*J'*r
    f = sum(r.^2)
endfunction

function D = lsqrsolveLM_scaleMatrix(J)
    // Parameters
    // D: a n-by-n matrix of doubles, the scaling matrix.
    // J: a m-by-n matrix of doubles, the jacobian matrix.
    //
    // Description
    // Update the scaling matrix according to Moré (1978)
    // If D == [], then set the initial scaling matrix.
    // If D <> [], then update the scaling matrix, depending on the 
    // scaling matrix of the previous iteration.
    Id = eye(n,n)
    //
    // Scale with J
    D = Id
    for i = 1 : n
        Ji = norm(Jopt(:,i))
        if ( Ji == 0 ) then
            D(i,i) = 1
        else
            D(i,i) = Ji
        end
    end
endfunction

function lsqrsolveLM_outputFun(niter,nevalf,fopt,gopt,Jopt,p,mu)
    if ( niter > 1 ) then
        plot(niter,mu,"bo")
        rk=rank(Jopt,sqrt(%eps))
        fmtmsg = "Iter=%3d, Funeval=%3d, F=%10s, ||G||=%10s, "+..
            "||p||=%s, mu=%10s, GR=%10s, Rk=%d\n"
        mprintf(fmtmsg, niter,nevalf,string(fopt),string(norm(gopt)),..
        string(norm(p)),string(mu),string(GR),rk)
    else
        mprintf("Iter=%3d, Funeval=%3d, F=%10s, ||G||=%10s, mu=%10s\n", ..
        niter,nevalf,string(fopt),string(norm(gopt)),string(mu))
    end
endfunction

function [r,J] = lsqrsolveLM_evalF (__LMcostf__,x)
    // Define my own cost function
    x = x(:)
    if ( typeof(__LMcostf__)=="function" ) then
        __LMcostf_f__ = __LMcostf__
        __LMcostf_args__ = list()
    elseif ( typeof(__LMcostf__)=="list" ) then
        __LMcostf_f__ = __LMcostf__(1)
        if ( typeof(__LMcostf_f__)<>"function" ) then
            lclmsg = "%s: Wrong type for element 1 in cost function.\n"
            error(msprintf(gettext(lclmsg),"lsqrsolveLM"))
        end
        __LMcostf_args__ = list(__LMcostf__(2:$))
    end
    [r, J] = __LMcostf_f__(x,__LMcostf_args__(1:$))
endfunction

function [stop,status] = lsqrsolveLM_terminate(niter,maxiter,nevalf,..
    maxfevals,xopt,p,fopt,atolx,rtolx,atolg)
    stop = %f
    status = ""
    if ( niter > 1 ) then
        // Terminate if necessary
        if ( niter > maxiter ) then
            status="maxiter"
            lclmsg = "%s: Maximum number of iterations reached.\n"
            warning(msprintf(gettext(lclmsg),"lsqrsolveLM"))
            stop = %t
        end
        if ( nevalf > maxfevals ) then
            status="maxevalf"
            lclmsg = "%s: Maximum number of function evaluations reached.\n"
            warning(msprintf(gettext(lclmsg),"lsqrsolveLM"))
            stop = %t
        end
        if ( norm(p) <= rtolx*norm(xopt) + atolx & norm(gopt) <= atolg ) then
            status="tolxg"
            stop = %t
        end
        if ( fopt==0 ) then
            status="tolf"
            stop = %t
        end
    end
endfunction

function GR = lsqrsolveLM_gainratio1(fopt,fnew,ropt,Jopt,D,p)
    // Compute the gain ratio with the naive formula
    funDecr = fopt - fnew
    modDecr = fopt - sum((ropt + Jopt*p).^2)
    if ( modDecr <> 0 ) then
        GR = funDecr/modDecr
    else
        GR = -%inf
    end
endfunction


function GR = lsqrsolveLM_gainratio2(fopt,fnew,ropt,Jopt,D,p)
    // Compute the gain ratio with a simplified formula for the 
    // decrease of the linear model
    funDecr = fopt - fnew
    modDecr = sum((Jopt*p).^2) + 2*mu*sum((D*p).^2)
    if ( modDecr <> 0 ) then
        GR = funDecr/modDecr
    else
        GR = -%inf
    end
endfunction

function GR = lsqrsolveLM_gainratio3(fopt,fnew,ropt,Jopt,D,p)
    // Compute the gain ratio with a floating point robust formula
    funDecr = 1 - fnew/fopt
    sqfpr = sqrt(fopt)
    modDecr = sum(((Jopt*p)/sqfpr).^2) + 2*mu*sum(((D*p)/sqfpr).^2)
    if ( modDecr <> 0 ) then
        GR = funDecr/modDecr
    else
        GR = -%inf
    end
endfunction

function p = lsqrsolveLM_computedir1(Jopt,mu,D,ropt,gopt)
    // Compute the search direction.
    // Use the normal equations.
    A = 2*Jopt'*Jopt
    B = A+2*mu*D^2
    p = B\-gopt
endfunction

function p = lsqrsolveLM_computedir2(Jopt,mu,D,ropt,gopt)
    // Compute the search direction.
    // Use the backslash operator on the least squares problem.
    m = warning("query")
    warning("off")
    // Solve the linear least squares problem:
    AL = [Jopt;sqrt(mu)*D]
    bL = [-ropt;zeros(n,1)]
    p = AL\bL
    warning(m)
endfunction
    // We could as well use the minimum norm solution :
    //
    // but this did not change anything in our experiments.

function p = lsqrsolveLM_computedir3(Jopt,mu,D,ropt,gopt)
    // Compute the search direction.
    // Use the lsq function, which returns the minimum 
    // norm solution.
    m = warning("query")
    warning("off")
    // Solve the linear least squares problem:
    AL = [Jopt;sqrt(mu)*D]
    bL = [-ropt;zeros(n,1)]
    p = lsq(AL,bL)
    warning(m)
endfunction
