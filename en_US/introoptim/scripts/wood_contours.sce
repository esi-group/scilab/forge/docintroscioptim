// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// wood --
//   Compute the Wood cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=2, returns the value of f,
//  * if ind=3, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   D. F. Shanno , K. H. Phua, 
//   Algorithm 500: Minimization of Unconstrained Multivariate 
//   Functions, 
//   ACM Transactions on Mathematical Software (TOMS), 
//   v.2 n.1, p.87-94, March 1976
//
//   Richard Brent,
//   Algorithms for Minimization without Derivatives, Prentice-Hall, 1973
//   Chapter 7, "A new algorithm for minimizing a function of 
//   several variables without calculating derivatives", p138, p141
//
//   Colville, 1968, A comparative study of nonlinear programming codes,
//   IBM New York Scientific Center Report 320 2949
//
//
//   http://people.sc.fsu.edu/~burkardt/f_src/test_opt/test_opt.f90
//
// Notes:
//   Starting point is x0 = [-3 -1 -3 -1]
//   f(x0)=   19192
//   g(x0) =  [-12008    -2080     -10808    -1880]
//   H(x0) = [
//     11202    1200         0        0         
//     1200.     220.2       0        19.8
//     0         0           10082    1080      
//     0         19.8        1080     200.2 
//     ]
//
//   Minimum is xopt = [1 1 1 1]
//   f(x0)=     0.0
//   
function [ f , g , H ] = wood ( x )
  A=x(2)-x(1)**2
  B=x(4)-x(3)**2
  //
  f=100.*A**2+(1.-x(1))**2+90.*B**2+(1.-x(3))**2+...
    10.1*((x(2)-1.)**2+(x(4)-1.)**2)+19.8*(x(2)-1.)*(x(4)-1.)
  //
  g(1)=-2.*(200.*x(1)*A+1.-x(1))
  g(2)=2.*(100.*A+10.1*(x(2)-1.)+9.9*(x(4)-1.))
  g(3)=-2.*(180.*x(3)*B+1.-x(3))
  g(4)=2.*(90.*B+10.1*(x(4)-1.)+9.9*(x(2)-1.))
  //
  H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  H(1,2) = -400 * x(1)
  H(1,3) = 0
  H(1,4) = 0
  H(2,1) = -400 * x(1)
  H(2,2) = 220.2
  H(2,3) = 0
  H(2,4) = 19.8
  H(3,1) = 0
  H(3,2) = 0
  H(3,3) = 1080*x(3)**2 - 360*x(4) + 2
  H(3,4) = -360 * x(3)
  H(4,1) = 0
  H(4,2) = 19.8
  H(4,3) = -360*x(3)
  H(4,4) = 200.2
endfunction

// Checking starting point
x0 = [-3 -1 -3 -1]';
[ f , g , H ] = wood ( x0 )

// Check derivatives at starting point
[gfd , Hfd ] = derivative ( wood , x0 , H_form='blockmat')
norm(g-gfd')/norm(g)
norm(H-Hfd)/norm(H)
spec(Hfd)
cond(Hfd)


// Check function value at optimum
xopt = [1 1 1 1]';
[ f , g ] = wood ( xopt );

// Check derivatives at optimum
[gfd , Hfd ] = derivative ( wood , xopt , H_form='blockmat')
spec(Hfd)
cond(Hfd)

// Plot the contour
function f = woodC12 ( x1 , x2 )
  f = wood ( [x1 x2 1 1] )
endfunction
function f = woodC13 ( x1 , x3 )
  f = wood ( [x1 1 x3 1] )
endfunction
function f = woodC14 ( x1 , x4 )
  f = wood ( [x1 1 1 x4] )
endfunction
function f = woodC23 ( x2 , x3 )
  f = wood ( [1 x2 x3 1] )
endfunction
function f = woodC24 ( x2 , x4 )
  f = wood ( [1 x2 1 x4] )
endfunction
function f = woodC34 ( x3 , x4 )
  f = wood ( [1 1 x3 x4] )
endfunction
//
scf();
xdata = linspace ( -4 , 2 , 100 );
ydata = linspace ( -2 , 2 , 100 );
contour ( xdata , ydata , woodC12 , [10 100 500 2000 20000] )
plot (   x0(1),   x0(2) , "bo")
plot ( xopt(1), xopt(2) , "r*")
//
scf();
xdata = linspace ( -4 , 2 , 100 );
ydata = linspace ( -4 , 2 , 100 );
contour ( xdata , ydata , woodC13 , [50 100 1000 5000 20000] )
plot (   x0(1),   x0(3) , "bo")
plot ( xopt(1), xopt(3) , "r*")
//
scf();
xdata = linspace ( -4 , 2 , 100 );
ydata = linspace ( -2 , 2 , 100 );
contour ( xdata , ydata , woodC14 , [10 50 100 300 1000 10000] )
plot (   x0(1),   x0(4) , "bo")
plot ( xopt(1), xopt(4) , "r*")
//
scf();
xdata = linspace ( -2 , 2 , 100 );
ydata = linspace ( -4 , 2 , 100 );
contour ( xdata , ydata , woodC23 , [10 50 100 300 1000 10000] )
plot (   x0(2),   x0(3) , "bo")
plot ( xopt(2), xopt(3) , "r*")
//
scf();
xdata = linspace ( 0 , 2 , 100 );
ydata = linspace ( 0 , 2 , 100 );
contour ( xdata , ydata , woodC24 , [5 20 40 80 120] )
plot (   x0(2),   x0(4) , "bo")
plot ( xopt(2), xopt(4) , "r*")
//
scf();
xdata = linspace ( 0 , 2 , 100 );
ydata = linspace ( 0 , 2 , 100 );
contour ( xdata , ydata , woodC34 , [1 10 50 200 400 700] )
plot (   x0(3),   x0(4) , "bo")
plot ( xopt(3), xopt(4) , "r*")

// Use subplots to display the contours
// Combinations : 12, 13, 14, 23, 24, 34
// X  12 13 14
// 21 X  23 24
// 31 32 X  34
// 41 42 43 X

// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction

c = combinations(4,2)

scf();
n = 4;
p = 1;
xmin = [-4 -2 -4 -2];
xmax = [2 2 2 2];
xcenter = [1 1 1 1];
funname = "wood";
nblevels = 5;
x0 = [-3 -1 -3 -1]';
xopt = [1 1 1 1]';
//vars = 1 : 3;
vars = [1 2 4];
nv = size ( vars , "*" )
for i = 1 : nv
  for j = 1 : nv
    if ( i <> j ) then
      p = (i-1)*nv + j;
      mprintf("i=%d, j=%d, p=%d\n",i,j,p)
      subplot ( nv , nv , p )
      xidata = linspace ( xmin(vars(i)) , xmax(vars(i)) , 100 );
      xjdata = linspace ( xmin(vars(j)) , xmax(vars(j)) , 100 );
      mprintf("x1 = [%f %f]\n",xmin(vars(i)) , xmax(vars(i)))
      mprintf("x2 = [%f %f]\n",xmin(vars(j)) , xmax(vars(j)))
      body = [
        "x = [" + strcat(string(xcenter)," ") + "]''"
        msprintf("x(%d)=xi",i)
        msprintf("x(%d)=xj",j)
        "y = " + funname + " ( x )"
      ];
      deff("y=myfContour(xi,xj)",body)
      contour ( xidata , xjdata , myfContour , nblevels )
	  xtitle("","X"+string(i),"X"+string(j))
      plot (   x0(vars(i)),   x0(vars(j)) , "bo")
      plot ( xopt(vars(i)), xopt(vars(j)) , "r*")
      h = gce();
      h.parent.data_bounds = [
        xmin(vars(i)) xmin(vars(j))
        xmax(vars(i)) xmax(vars(j))
      ];
    end
  end
end

