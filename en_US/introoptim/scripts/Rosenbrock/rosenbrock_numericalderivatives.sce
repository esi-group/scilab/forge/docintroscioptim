// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

x0 = [-1.2 1.0];

//***************************************************
// Test with numerical derivatives - "derivative"
function f = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [ f , g , ind ] = cost ( x , ind )
  if ind == 2 | ind == 4 then
    f = rosenbrock ( x )
  end
  if ind == 2 | ind == 4 then
    g = derivative(rosenbrock,x')
  end
endfunction
[ fopt , xopt ] = optim ( cost , x0 )
// fopt = 5.332D-17

//***************************************************
// Test with numerical derivatives - fine tuning
function f = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [ f , g , ind ] = cost ( x , ind )
  if ind == 2 | ind == 4 then
    f = rosenbrock ( x )
  end
  if ind == 2 | ind == 4 then
    g = derivative(rosenbrock,x' , order= o)
  end
endfunction
xref = [1  1]
for o = [1 2 4]
  [ fopt , xopt ] = optim ( cost , x0 ); 
  mprintf("%d %e %e\n" , o , norm(xopt-xref) , fopt );
end
// o |xopt-xref|   fopt
//-----------------------------
// 1 1.001916e-005 2.005565e-011
// 2 1.632177e-008 5.331635e-017
// 4 8.043564e-015 1.385437e-029

//***************************************************
// Test with numerical derivatives - "numdiff"
function f = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [ f , g , ind ] = cost ( x , ind )
  if ind == 2 | ind == 4 then
    f = rosenbrock ( x )
  end
  if ind == 2 | ind == 4 then
    g = numdiff(rosenbrock,x')
  end
endfunction
[ fopt , xopt ] = optim ( cost , x0 )
// fopt = 2.010D-11


