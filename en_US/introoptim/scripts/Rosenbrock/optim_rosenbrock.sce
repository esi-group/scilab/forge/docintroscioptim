// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// rosenbrock --
//   Compute the Rosenbrock cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=1, returns the value of f,
//  * if ind=2, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   D. F. Shanno , K. H. Phua, 
//   Algorithm 500: Minimization of Unconstrained Multivariate 
//   Functions, 
//   ACM Transactions on Mathematical Software (TOMS), 
//   v.2 n.1, p.87-94, March 1976
//
//   Richard Brent,
//   Algorithms for Minimization with Derivatives
//
//   http://people.sc.fsu.edu/~burkardt/f_src/test_opt/test_opt.f90
//
// Notes:
//   Starting point is x0 = [-1.2 1.0]
//   f(x0)=   24.2
//   Minimum is xopt = [1 1]
//   f(x0)=   0.
//   
function [ f , g , ind ] = rosenbrock ( x , ind )
  if ind == 2 | ind == 4 then
    f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  end
  if ind == 2 | ind == 4 then
    g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
    g(2) = 200. * ( x(2) - x(1)**2 )
  end
endfunction

// Check the function value and derivatives at initial guess
function f = rosenbrockfornumdiff ( x )
  f = rosenbrock ( x , 2 )
endfunction
x0 = [-1.2 1.0];
[ f , g ] = rosenbrock ( x0 , 4 );
mprintf ( "Computed f(x0) = %f (expected = %f)\n", f, 24.2 );
mprintf ( "Computed g(x0) = \n");disp(g');
mprintf ( "Expected g(x0) = \n");disp(derivative(rosenbrockfornumdiff,x0'))

// Call without arguments
[ fopt , xopt ] = optim ( rosenbrock , x0 )


// Display all digits
format(25)
[ fopt , xopt ] = optim ( rosenbrock , x0 )
format(10)

// Get gopt
[ fopt , xopt , gopt ] = optim ( rosenbrock , x0 )

// Check the hessian at optimum
[J , H ] = derivative(rosenbrockfornumdiff , xopt' , H_form='blockmat')
spec(H)

// Draw the contour of Rosenbrock's function
xmin = -2.0
xmax = 2.0
stepx = 0.1
ymin = -1.0
ymax = 2.0
stepy = 0.1
nx = 100
ny = 100
stepx = (xmax - xmin)/nx;
xdata = xmin:stepx:xmax;
stepy = (ymax - ymin)/ny;
ydata = ymin:stepy:ymax;
for ix = 1:length(xdata)
    for iy = 1:length(ydata)
      x = [xdata(ix) ydata(iy)];
      f = rosenbrock ( x , 2 );
      zdata ( ix , iy ) = f;
    end
end
contour ( xdata , ydata , zdata , [1 10 100 500 1000])
plot(x0(1) , x0(2) , "b.")
plot(xopt(1) , xopt(2) , "r*")

// Optimize, store the intermediate points and display them on the 
// contour plot.
function [ f , g , ind ] = rosenbrock ( x , ind )
  if ind == 2 | ind == 4 then
    f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  end
  if ind == 2 | ind == 4 then
    g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
    g(2) = 200. * ( x(2) - x(1)**2 )
  end
  if ind == 1 then
    plot ( x(1) , x(2) , "g." )
  end
endfunction
[ fopt , xopt ] = optim ( rosenbrock , x0 , imp = -1)

// Print out maximum level of details.
consolebox("on")
[ fopt , xopt , gradopt ] = optim ( rosenbrock , x0 , imp = 4 )

// Test with numerical derivatives
function f = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [ f , g , ind ] = cost ( x , ind )
  if ind == 2 | ind == 4 then
    f = rosenbrock ( x )
  end
  if ind == 2 | ind == 4 then
    g = derivative(rosenbrock,x')
  end
  if ind == 1 then
    plot ( x(1) , x(2) , "g." )
  end
endfunction
contour ( xdata , ydata , zdata , [1 10 100 500 1000])
[ fopt , xopt ] = optim ( cost , x0 , imp = -1 )

// Test with numerical derivatives - fine tuning
function f = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [ f , g , ind ] = cost ( x , ind )
  if ind == 2 | ind == 4 then
    f = rosenbrock ( x )
  end
  if ind == 2 | ind == 4 then
    g = derivative(rosenbrock,x' , order= o)
  end
endfunction
xref = [1  1]
for o = [1 2 4]
  [ fopt , xopt ] = optim ( cost , x0 ); 
  mprintf("%d %e %e\n" , o , norm(xopt-xref) , fopt );
end

//***************************************************
// Tuning termination criteria

function [ f , g , ind ] = myquadratic ( x , ind )
  n = 10
  if ind == 2 | ind == 4 then
    f = x(1)^n + x(2)^n;
  end
  if ind == 2 | ind == 4 then
    g(1) = n * x(1)^(n-1)
    g(2) = n * x(2)^(n-1)
  end
  if ind == 1 then
    f = x(1)^n + x(2)^n;
    g(1) = n * x(1)^(n-1)
    g(2) = n * x(2)^(n-1)
    ng = norm(g)
    mprintf("f=%e, |g|=%e\n",f,ng)
  end
endfunction
x0 = [-1.2 1.0];
nap = 100
iter = 100
epsg = %eps
// epsf and epsx are ignored in this case
[ fopt , xopt , gradopt ] = optim ( myquadratic , x0 , "ar" , nap , iter , epsg , imp = -1)
epsg = 0.1
[ fopt , xopt , gradopt ] = optim ( myquadratic , x0 , "ar" , nap , iter , epsg , imp = -1)
//***************************************************

