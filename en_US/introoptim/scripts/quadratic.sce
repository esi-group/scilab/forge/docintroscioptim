// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//**********************************************************
// Draw the contours of a well conditionned quadractic function
//

function [f,g,H] = quadratic ( x )
  H = [2 1;1 4]
  b = [1;2]
  f = b' * x + 0.5 * x' * H * x
  g = b + H * x
endfunction

function f = quadraticC ( x1 , x2 )
  f = quadratic ( [x1 x2]' )
endfunction
xdata = linspace(-2,2,100);
ydata = linspace(-2.5,1.5,100);
contour ( xdata , ydata , quadraticC , [0.5 2 4 8 12] )
xtitle("A quadratic function","X1","X2")

x = [1;1]
[ f , g , H ] = quadratic ( x )
[gfd , Hfd ] = derivative ( quadratic , x , H_form='blockmat')
norm(g-gfd')/norm(g)
norm(H-Hfd)/norm(H)

//
// Plot gradient at [1,x2] for various values of x2
for x2 = [0.5 0 -0.5 -1 -1.5]
x = [1;x2];
[ f , g , H ] = quadratic ( x );
plot(x(1),x(2),"bo")
v = g / norm(g);
data = [x x+v];
plot(data(1,:),data(2,:),"r-")
plot(data(1,2),data(2,2),"k+")
end
h = gcf();
h.children.isoview="on";

