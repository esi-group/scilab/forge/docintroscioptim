// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// An example where the function is non convex, 
// but the set of points f(x)>=0 is convex.
// This is the answer to the question : what does a non-convex function
// look like ?

// Reference
// Stephen Boyd, Convex Optimization I, Lecture 5

// Consider the function
// f(x1,x2) = x1/(1+x2^2)
// The set of points satisfying x1/(1+x2^2) >= 0 is a convex set.
// It is simply x1>=0.
// But the function f is non convex.

function z = f ( x1 , x2 ) 
  z = x1./(1+x2^2)
endfunction

x = linspace ( -5,5,100);
y = linspace ( -5,5,100);
//contour(x,y,f,20)
contour(x,y,f,[-5 -3 -2 -1 -0.3 0.3 1 2 3 5])

// A surface
x = linspace ( -5,5,20);
y = linspace ( -5,5,20);
Z = (eval3d(f,x,y))';
surf(x,y,Z)
h = gcf();
cmap=graycolormap(10);
h.color_map = cmap;

// Plot a cut of the surface along the line from (-5,-1) to (5,1)
// This is not convex !
x1 = -5
y1 = -1
x2 = 5
y2 = 1
for a = linspace ( 0 , 1 , 100 )
  x = a * x1 + (1-a)*x2;
  y = a * y1 + (1-a)*y2;
  z = f ( x , y );
  plot(a,z,"bo");
end


// Search a point on the line s=(x1,y1) to e=(x2,y2) where f is not convex
// Uses n points in the search
// Returns the a in ]0,1[ and w=(x,y) coordinates of the point where the condition fails, where 
// w = a * s + (1-a)*e
// and f(w)>a * f(s) + (1-a)*f(e).
// If no point is found, returns a = 1 and w = [].
function [a, w] = searchnonconvex ( s , e , f , n )
z1 = f ( s(1) , s(2) )
z2 = f ( e(1) , e(2) )
for a = linspace ( 0 , 1 , n )
  x = a * s(1) + (1-a)*e(1)
  y = a * s(2) + (1-a)*e(2)
  z = f ( x , y )
  zp = a * z1 + (1-a)*z2
  if ( zp < z ) then
    w = [x y]
    break
  end
end
w = []
endfunction

// Search on the line (-5,0) to (5,0) : good
// As expected, on this line, z = x1
[a w] = searchnonconvex ( [-5 0] , [5 0] , f , 100 )

// Search on the line (0,-5) to (0,5) : good
// As expected, on this line, z = 0 !
[a w] = searchnonconvex ( [0 -5] , [0 5] , f , 100 )

// Search on the line (-5,1) to (5,1) : non convex
[a w] = searchnonconvex ( [-5 1] , [5 1] , f , 100 )
    mprintf("f is not convex!\n")
    mprintf("a=%f\n",a)
    mprintf("x=%f, y=%f\n",x,y)
    mprintf("z=%f, zp=%f\n",z,zp)

// Find two particular points a,b
a = [1 ; -1];
b = [1 ; 1];
t = 0.5;
c = t*a + (1-t)*b;
fa = f(a(1),a(2));
fb = f(b(1),b(2));
fc = f(c(1),c(2));
[ fc t*fa+(1-t)*fb]
 

