// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//**********************************************************
// Draw the contours of a quadractic function
//
// Test #1 : simple call
function f = myquadratic ( x1 , x2 )
  f = x1**2 + x2**2;
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , myquadratic , 10)
xs2png(0,"introoptim-testcontour1.png")
clf();
//
// Test #2 : practical use with computation of the data
function f = myquadratic2 ( x1 , x2 )
  x = [x1 x2]
  f = x(1)**2 + x(2)**2;
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , myquadratic2 , [0.1 0.3 0.5 0.7])
xs2png(0,"introoptim-testcontour2.png")
//
// Test #3 : practical use with computation of the data
function f = myquadratic2 ( x )
  f = x(1)**2 + x(2)**2;
endfunction
function f = myquadratictwoargs ( x1 , x2 )
  x = [x1 x2]
  f = myquadratic2(x);
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , myquadratictwoargs , [0.1 0.3 0.5 0.7])
xs2png(0,"introoptim-testcontour3.png")
clf();

//**********************************************************
//**********************************************************
// Draw the contours of a quadractic associated with
// an indefinite Hessian.
function f = quadraticsaddle ( x1 , x2 )
  x = [x1 x2 ]'
  H = [3 -1; -1 -8]
  f = x' * (H * x)
endfunction
x = linspace(-2,2,100);
y = linspace(-2,2,100);
contour ( x , y , quadraticsaddle , ..
  [-20 -10 -5 -0.3 0.3 2 5 10])
xs2png(0,"introoptim-saddlecontour.png")
clf();
function f = quadraticsaddle ( x1 , x2 )
  x = [x1' x2']'
  H = [3 -1; -1 -8]
  y = H * x
  n = size(y,"c")
  for i = 1 : n
    f(i) = x(:,i)' * y(:,i)
  end
endfunction
x = linspace(-2,2,20);
y = linspace(-2,2,20);
Z = (eval3d(quadraticsaddle,x,y))';
surf(x,y,Z)
h = gcf();
cmap=graycolormap(10);
h.color_map = cmap;
xs2png(0,"introoptim-saddle3d.png")
clf();
//**********************************************************
//**********************************************************
// Compute the eigenvalues of a symetric positive definite 
// Hessian matrix
H = [5 3; 3 2]
[R , D] = spec(H)
R.' * R
//**********************************************************
//**********************************************************
// Draw the contours of a quadractic associated with
// an positive definite Hessian.
function f = quadraticdefpos ( x1 , x2 )
  x = [x1 x2]'
  H = [5 3; 3 2]
  f = x.' * H * x;
endfunction
x = linspace(-2,2,100);
y = linspace(-2,2,100);
contour ( x , y , quadraticdefpos , [0.3 2 5 10 20])
xs2png(0,"introoptim-contourdefpos.png")
//**********************************************************
//**********************************************************
// Draw the contours of a quadractic associated with
// an indefinite definite Hessian, with one zero eigenvalue and the other 
// is positive.
function f = quadraticindef ( x1 , x2 )
  x = [x1 x2]'
  H = [4 2; 2 1]
  f = x.' * H * x;
endfunction
x = linspace(-2,2,20);
y = linspace(-2,2,20);
contour ( x , y , quadraticindef , [0.3 2 5 10 20])
xs2png(0,"introoptim-contournotdef.png")
//**********************************************************
//**********************************************************
// Draw the contours of a quadractic associated with
// an incompatible linear system for the stationnary point.
function f = quadraticincomp ( x1 , x2 )
  x = [x1 x2]'
  H = [0 0;0 1]
  b = [1;0]
  f = x.'*b + x.' * H * x;
endfunction
x = linspace(-10,10,100);
y = linspace(-10,10,100);
contour ( x , y , quadraticincomp , [-10 -5 0 5 10 20])
xs2png(0,"introoptim-contourincomp.png")
clf();
//**********************************************************

