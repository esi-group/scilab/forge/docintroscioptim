// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// References
// Practical Optimization,
// Gill, Murray, Wright, 1981

// Section 8.5. 
// ESTIMATING THE ACCURACY OF THE PROBLEM FUNCTIONS

// Let x be the exact mathematical number 
// and fl(x) its floating point representation.
// We wish to compute the absolute error e such that 
//
// | fl( f( fl(x))) - f(x) | <= e
//
// that is, the absolute error in the computed function 
// value.

// Section 8.5.2.4, Numerical Examples

function y = myfun(x)
    y = exp(x) + x^3 - 3*x - 1.1
endfunction

function y = myfunp(x)
    y = exp(x) + 3*x^2 - 3
endfunction

function evaluateMethods(x,fexact,gexact,rtol,fun,funp)
    mprintf("x=%s\n",string(x));
    mprintf("Checking F...\n");
    f = fun(x)
    mprintf("\t f(x)= %s\n",string(f));
    assert_checkalmostequal(f,fexact,rtol);
    mprintf("Checking G...\n");
    g = funp(x);
    mprintf("\t g(x)= %s\n",string(g));
    assert_checkalmostequal(g,gexact,rtol);
    gFD = derivative(fun,x)
    abs(gFD-g)/abs(g)
    assert_checkalmostequal(g,gFD,rtol);
    // Section 8.5.1.3
    // Standard lower bound on e
    mprintf("Method #1: Standard lower bound\n")
    mprintf("\t Approximate Absolute Error in F\n")
    e = %eps*(1+abs(f));
    mprintf("\t Std Error: %s\n",string(e))
    mprintf("\t Relative Error: %s\n",string(e/abs(f)))
    // Method of Section 8.5.2.2. 
    // Estimating the accuracy when derivatives are available
    mprintf("Method #2: when fprime is available\n")
    h = %eps*abs(x);
    mprintf("\t hmin: %s\n",string(h));
    e = abs(fun(x+h)-f);
    mprintf("\t |f(x+h)-f(x)|: %s\n",string(e));
    e = h * abs(g);
    mprintf("\t e = h*|g|: %s\n",string(e));
    mprintf("\t Relative Error: %s\n",string(e/abs(f)))
    // Method of section 8.5.2.3
    // Estimating the accuracy when only function values 
    // are available
    mprintf("Method #3: when f only is available\n")
    h = diffstepmin(x,fun)
    h = sqrt(h)
    k = 5
    T = difftable(x,h,k,fun)
    mprintf("Difference Table:\n");
    //disp(T(1:k,1:k))
    disp(size(T))
    disp(T)
    betak=sqrt(factorial(2*k)/(factorial(k)^2))
    e = max(abs(T(1:k,k)))/betak
    mprintf("\t e: %s\n",string(e))
    mprintf("\t Relative Error: %s\n",string(e/abs(f)))
endfunction

function T = difftable(x,h,k,fun)
    // Compute a difference table at point x, 
    // with step h, 
    // with n points and k levels.
    // We expect that n=2*k-1.
    
    n = 2*k-1
    //
    // Level j = 1
    j = 1
    for i = 1 : n
        T(i,j) = fun(x+(i-1)*h)
    end
    //
    // Level j = 2, 3, ..., k
    i = n
    for j = 2 : k
        T(1:i-1,j) = T(2:i,j-1) - T(1:i-1,j-1)
        i = i-1
    end
endfunction

function h = diffstepmin(x,fun)
    // Returns the minimum step which measures a 
    // variation in f
    f = fun(x)
    xnew = nearfloat("succ",x)
    fnew = fun(xnew)
    h = xnew - x
    for i = 1 : 17
        if ( fnew<>f ) then
            break
        end
        h = 2 * h
        xnew = x + h
        fnew = fun(xnew)
    end
    if ( i == 17) then
        h = []
    end
endfunction

// Checking the derivative at point #1
// In the book: 
// f(x1) = 2.29954 X 10^4
// f'(x1) = 2.23235 X 10^4
x = 10;
fexact = 2.29954e4;
gexact = 2.23235e4;
rtol = 1.e-4;
mprintf("\nPoint #1\n");
evaluateMethods(x,fexact,gexact,rtol,myfun,myfunp);

// Checking the derivative at point #2
// In the book:
// f(x2) = -9.23681 X 10^-3
// f'(x2) = 3.77924
x = 1.115;
fexact = -9.23681e-3;
gexact = 3.77924;
rtol = 1.e-4;
mprintf("\nPoint #2\n");
evaluateMethods(x,fexact,gexact,rtol,myfun,myfunp);

// Checking the derivative at point #3
// In the book:
// f(x2) = not given
// f'(x2) = not given
x = 1.e-2;
fexact = myfun(x);
gexact = myfunp(x);
rtol = 1.e-4;
mprintf("\nPoint #3\n");
evaluateMethods(x,fexact,gexact,rtol,myfun,myfunp);

// Checking the derivative at point #4
// In the book:
// f(x2) = not given
// f'(x2) = not given
x = 1.e2;
fexact = myfun(x);
gexact = myfunp(x);
rtol = 1.e-4;
mprintf("\nPoint #4\n");
evaluateMethods(x,fexact,gexact,rtol,myfun,myfunp);


///////////////////////////////////////////////////
// 
// Try with exp in the neighbourhood of x=0
//

// No method seem to be able to reveal the 
// difficulty of computing accurately exp(x) 
// in the neighbourhood of x=0.
// Actually, the rounding error is low for this 
// x.

function y = myexp(x)
    y = exp(x)
endfunction

x = 1.e-10;
fexact = myexp(x);
gexact = myexp(x);
rtol = 1.e-4;
mprintf("\nExp\n");
evaluateMethods(x,fexact,gexact,rtol,myexp,myexp);
//

