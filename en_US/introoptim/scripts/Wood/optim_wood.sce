// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// wood --
//   Compute the Wood cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=1, returns the value of f,
//  * if ind=2, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   Testing Unconstrained Optimization Software
//   J. J. Mor�, Burton S. Garbow, Kenneth E. Hillstrom
//   ACM Transactions on Mathematical Software (TOMS)
//   Volume 7 Issue 1, March 1981 
//
//   D. F. Shanno , K. H. Phua, 
//   Algorithm 500: Minimization of Unconstrained Multivariate 
//   Functions, 
//   ACM Transactions on Mathematical Software (TOMS), 
//   v.2 n.1, p.87-94, March 1976
//
//   Richard Brent,
//   Algorithms for Minimization without Derivatives, Prentice-Hall, 1973
//   Chapter 7, "A new algorithm for minimizing a function of 
//   several variables without calculating derivatives", p138, p141
//
//   A. R. Colville, 1968, A comparative study of nonlinear programming codes,
//   IBM New York Scientific Center Report 320 2949
//
//   http://people.sc.fsu.edu/~burkardt/f_src/test_opt/test_opt.f90
//
// Notes:
//   Starting point is x0 = [-3 -1 -3 -1]
//   f(x0)=   19192.0
//   Minimum is xopt = [1 1 1 1]
//   f(x0)=     0.0
//   


function [ f , g , ind ] = wood ( x , ind )
  if ind == 2 | ind == 3 | ind == 4 then
    A=x(2)-x(1)^2
    B=x(4)-x(3)^2
  else
    error ( mprintf ( "Unexpected value of index %d", ind ))  
  end
  if ind == 2 | ind == 4 then
    f=100*A^2+(1-x(1))^2+90*B^2+(1-x(3))^2+...
      10.1*((x(2)-1.)^2+(x(4)-1.)^2)+19.8*(x(2)-1)*(x(4)-1)
  end
  if ind == 3 | ind == 4 then
    g(1)=-2.*(200*x(1)*A+1.-x(1))
    g(2)=2.*(100*A+10.1*(x(2)-1.)+9.9*(x(4)-1.))
    g(3)=-2.*(180*x(3)*B+1.-x(3))
    g(4)=2.*(90*B+10.1*(x(4)-1.)+9.9*(x(2)-1.))
  end
endfunction


x0 = [-3 -1 -3 -1];
// Check the function value at initial guess
[ f , g ] = wood ( x0 , 4 );
mprintf ( "Computed f(x0) = %f (expected = %f)\n", f, 19192.0);
// Check the derivative
mprintf ( "Computed g(x0) = \n");
disp(g);
mprintf ( "Expected g(x0) = \n");
function f = woodfornumdiff ( x )
  f = wood ( x , 2 )
endfunction
disp(numdiff(woodfornumdiff,x0))
disp(derivative(woodfornumdiff,x0'))

consolebox("on")
// Use Unconstrained Quasi-Newton BFGS
nap = 1000
iter = 100
[ fopt , xopt ,gradopt ] = optim ( wood , [-3 -1 -3 -1] , "qn" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-3 1 -3 1] , "qn" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-1.2 1 -1.2 1] , "qn" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-1.2 1 1.2 1] , "qn" , "ar" , nap , iter , imp = 1 )
// Use Unconstrained Quasi-Newton L-BFGS
nap = 1000
iter = 100
[ fopt , xopt ,gradopt ] = optim ( wood , [-3 -1 -3 -1] , "gc" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-3 1 -3 1] , "gc" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-1.2 1 -1.2 1] , "gc" , "ar" , nap , iter , imp = 1 )
[ fopt , xopt ,gradopt ] = optim ( wood , [-1.2 1 1.2 1] , "gc" , "ar" , nap , iter , imp = 1 )

