Documentation : Introduction to Unconstrained Optimization with Scilab

Abstract

This document is a small introduction to unconstrained optimization 
optimization with Scilab. 
In the first section, we analyze optimization problems and define the associated vocabu-
lary. We introduce level sets and separate local and global optimums. We emphasize
the use of contour plots in the context of unconstrained and constrained optimiza-
tion.
In the second section, we present the definition and properties of convex sets and convex
functions. Convexity dominates the theory of optimization and a lot of theoretical
and practical optimization results can be established for these mathematical objects.
We show how to use Scilab for these purposes. 
We show how to define and validate an implementation of Rosenbrock's 
function in Scilab. 
We present methods to compute first and second numerical derivatives with 
the derivative function. 
We show how to use the contour function in order 
to draw the level sets of a function. 
Exercises (and their answers) are provided.

Author

Copyright (C) 2008-2010 - Michael Baudin

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/

TODO
 * Add a section on eigenvalues and positive-definite matrices.
   This should be just after level sets.
 * Add a section on stationnary points.
